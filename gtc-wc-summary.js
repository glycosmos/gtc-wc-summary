import {LitElement, html} from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';


class GtcWcSummary extends LitElement {
  static get properties() {
    return {
      sampleids: Array,
      accession: String
    };
  }

  render() {
    return html `
<style>
  table {
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    border: solid 2px #CCC;
  }

  th {
    width: 25%;
  }

  th, td {
    padding: 5px;
    text-align: left;
    border: solid 2px #CCC;
    font-size: 14px;
    line-height: 1.5;
    font-weight: normal;
  }
</style>
  <div id="summary">${this._processHtml()}<div>
   `;
  }

  constructor() {
    super();
    this.accession="";
    this.image="";
    this.sampleids=null;
    this.mass=null;
    this.contributionTime=null;
  }

  connectedCallback() {
    super.connectedCallback();
    const host =  getdomain(location.href);
    const url = 'https://'+host+'/sparqlist/api/gtc_summary?accNum=' + this.accession;
    this.getSummary(url);
  }

  async getSummary(url) {
    await fetch(url,{
      mode: 'cors'
    }).then(res => {
      if(res.ok) {
        return res.json();
      } else {
        throw new Error();
      }
    }).then(resArr => {
      this.sampleids = resArr;
    }).catch(err => {
      this.sampleids = null;
    });
    await this.requestUpdate();
  }



  _processHtml() {
    if(this.sampleids[0].ContributionTime === undefined) {
      // Could not retrieve ContributionTime
      return html`
      <table>
        <tr><th>GlyCosmos Entry</th><td><a href="https://glycosmos.org/glycans/show/${this.sampleids[0].AccessionNumber}" target="_blank">${this.sampleids[0].AccessionNumber}</a></td></tr>
      </table>
      `;
    } else {
      // Could retrieve ContributionTime
      var popped = this.sampleids.pop();
      var date = popped.ContributionTime;
      date = new Date(date.split(" ")[0]).toISOString();
      date = date.split("T")[0];
      console.log(date);
      return html`
      <table>
        <tr><th>GlyCosmos Entry</th><td><a href="https://glycosmos.org/glycans/show/${popped.AccessionNumber}" target="_blank">${popped.AccessionNumber}</a></td></tr>
        <tr><th>Created Date</th><td>${date}</td></tr>
      </table>
      `;

    }
  }
}

customElements.define('gtc-wc-summary', GtcWcSummary);
