# Components

GitLab CI works on this project so check the Gitlab [Page]().

# Tutorial

Install node packages.
```
$ npm i
```

A main webcomponent file is in public/ directory. To update public, enter webpack command.
```
$ webpack
```

Open the index on a browser
```
$ open public/index.html
```
